package com.nlmk.shokin;

import com.nlmk.shokin.model.Person;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        // write your code here
        Parser parser = new Parser();
        List<Object> personList = new ArrayList<>();
        personList.add(new Person("Michael", "Scott"));
        personList.add(new Person("Dwight", "Schrute", LocalDate.of(1980, 1, 10)));
        personList.add(new Person("James", "Halpert", LocalDate.of(1996, 10, 10), "mail@gmail.com"));

        try {
            parser.parseToCSV(personList, "person.csv");
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
        LOGGER.info("File successfully created");
    }

}