package com.nlmk.shokin.model;

import java.io.Serializable;
import java.time.LocalDate;

public class Person implements Serializable {

    private String firstName;
    private String lastName;
    private LocalDate birthDay;
    private String email;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(String firstName, String lastName, LocalDate birthDay) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
    }

    public Person(String firstName, String lastName, LocalDate birthDay, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.email = email;
    }

}