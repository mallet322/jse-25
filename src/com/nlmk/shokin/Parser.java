package com.nlmk.shokin;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.logging.Logger;

public class Parser {

    private static final char SEPARATOR = ',';
    private static final Logger LOGGER = Logger.getLogger(Parser.class.getName());

    public void parseToCSV(List<?> objectList, String fileName) throws IOException {

        Class<?> entity = objectList.get(0).getClass();

        for (Object object : objectList) {
            if (object.getClass() != entity) {
                throw new IllegalArgumentException("Objects with different types!");
            }

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));

            StringBuilder result = new StringBuilder();
            if (objectList.isEmpty()) return;

            result.append(serializeHeader(objectList.get(0)));
            result.append("\n");

            for (Object obj : objectList) {
                result.append(serializeRow(obj)).append("\n");
            }

            writer.write(result.toString());
            writer.flush();
            writer.close();
        }
    }

    public String serializeHeader(Object obj) {
        StringBuilder header = new StringBuilder();
        boolean firstObject = true;
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                String value = field.getName();
                if (firstObject) {
                    header.append(value);
                    firstObject = false;
                } else {
                    header.append(SEPARATOR).append(value);
                }
            } catch (IllegalArgumentException e) {
                LOGGER.severe(e.getMessage());
            }
        }
        return header.toString();
    }

    public String serializeRow(Object obj) {
        StringBuilder row = new StringBuilder();
        Field[] fields = obj.getClass().getDeclaredFields();
        boolean firstObject = true;
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object value = field.get(obj);
                if (value == null) {
                    value = "null";
                }
                if (firstObject) {
                    row.append(value);
                    firstObject = false;
                } else {
                    row.append(SEPARATOR).append(value);
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                LOGGER.severe(e.getMessage());
            }
        }
        return row.toString();
    }

}